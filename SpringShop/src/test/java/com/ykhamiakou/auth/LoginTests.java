package com.ykhamiakou.auth;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LoginTests {
    
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testNoProductAuth() throws Exception {
        this.mockMvc.perform(get("/product"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testProductAuth() throws Exception {
        this.mockMvc.perform(get("/product").with(httpBasic("admin", "root")))
                .andExpect(status().isOk());
    }

    @Test
    public void testNoCategoryAuth() throws Exception {
        this.mockMvc.perform(get("/category"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testCategoryAuth() throws Exception {
        this.mockMvc.perform(get("/category").with(httpBasic("admin", "root")))
                .andExpect(status().isOk());
    }

    @Test
    public void testNoPriceAuth() throws Exception {
        this.mockMvc.perform(get("/price"))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void testPriceAuth() throws Exception {
        this.mockMvc.perform(get("/price").with(httpBasic("admin", "root")))
                .andExpect(status().isOk());
    }
}
