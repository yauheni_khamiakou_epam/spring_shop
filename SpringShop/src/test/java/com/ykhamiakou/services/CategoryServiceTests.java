package com.ykhamiakou.services;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.service.CategoryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTests {
    @Autowired
    private CategoryService service;

    @Test
    public void testAdd() {
        String name = "someName";
        Category category = new Category(name, null);
        this.service.addCategory(category);
        Optional<Category> executed = this.service.findCategoryByName(name);
        Assert.assertEquals(category.getName(), executed.get().getName());
        this.service.deleteCategory(executed.get());
    }

    @Test
    public void testDelete() {
        String name = "someName";
        Category category = new Category(name, null);
        this.service.addCategory(category);
        Optional<Category> executed = this.service.findCategoryByName(name);
        this.service.deleteCategory(executed.get());
        Assert.assertEquals(Optional.empty(), this.service.findCategoryByName(name));
    }
}
