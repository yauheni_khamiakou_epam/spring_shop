package com.ykhamiakou.services;

import com.ykhamiakou.entity.Product;
import com.ykhamiakou.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {
    @Autowired
    private ProductService service;

    @Test
    public void testAdd() {
        String name = "someName";
        Product product = new Product(name);
        this.service.addProduct(product);
        Optional<Product> executed = this.service.findProductByName(name);
        Assert.assertEquals(product.getName(), executed.get().getName());
        this.service.deleteProduct(executed.get());
    }

    @Test
    public void testDelete() {
        String name = "someName";
        Product product = new Product(name);
        this.service.addProduct(product);
        Optional<Product> executed = this.service.findProductByName(name);
        this.service.deleteProduct(executed.get());
        Assert.assertEquals(Optional.empty(), this.service.findProductByName(name));
    }
}
