package com.ykhamiakou.controller;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.exception.CategoryNotFoundException;
import com.ykhamiakou.service.impl.CategoryServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Category controller
 */
@RestController
public class CategoryController {

    @Autowired
    private CategoryServiceImpl categoryService;

    @GetMapping("/category")
    public Page<Category> all(Pageable pageable) {
        return this.categoryService.getAllCategories(pageable);
    }

    @GetMapping("/category/search/byId/{id}")
    public Category findCategoryById(@PathVariable Long id) throws CategoryNotFoundException {
        return this.categoryService.findCategoryById(id).orElseThrow(()-> new CategoryNotFoundException(id));
    }

    @GetMapping("/category/search/byName/{name}")
    public Category findCategoryByName(@PathVariable String name) throws CategoryNotFoundException {
        return this.categoryService.findCategoryByName(name).orElseThrow(()-> new CategoryNotFoundException(name));
    }

    @PostMapping("/category")
    public Category newPrice(@RequestBody Category category) {
        return this.categoryService.addCategory(category);
    }

    @DeleteMapping("/category/{id}")
    public void deletePrice(@PathVariable Long id) {
        this.categoryService.deleteCategoryById(id);
    }

    @PutMapping("/category")
    public Category replacePrice(@RequestBody Category category) {
        return this.categoryService.updateCategory(category);
    }
}
