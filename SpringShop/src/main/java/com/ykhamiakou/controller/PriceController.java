package com.ykhamiakou.controller;

import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import com.ykhamiakou.service.impl.PriceServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Price controller
 */
@RestController
public class PriceController {

    @Autowired
    private PriceServiceImpl priceService;

    @GetMapping("/price")
    public Page<Price> all(Pageable pageable) {
        return this.priceService.getAllPrices(pageable);
    }

    @GetMapping("/price/search")
    public Page<Price> findPriceBetween(@RequestParam BigDecimal from, @RequestParam BigDecimal to, Pageable pageable) {
        return this.priceService.findPriceByValueBetween(from, to, pageable);
    }

    @PostMapping("/price/search/byProduct")
    public Page<Price> findByProduct(@RequestBody Product product, Pageable pageable) {
        return this.priceService.findPriceByProducts(product, pageable);
    }

    @GetMapping("/price/search/byCurrency/{currency}")
    public Page<Price> findByCurrency(@PathVariable Currency currency, Pageable pageable) {
        return this.priceService.findPriceByCurrency(currency, pageable);
    }

    @PostMapping("/price")
    public Price newPrice(@RequestBody Price price) {
        return this.priceService.addPrice(price);
    }

    @DeleteMapping("/price/{id}")
    public void deletePrice(@PathVariable Long id) {
        this.priceService.deletePriceById(id);
    }

    @PutMapping("/price")
    public Price replacePrice(@RequestBody Price price) {
        return this.priceService.updatePrice(price);
    }

}
