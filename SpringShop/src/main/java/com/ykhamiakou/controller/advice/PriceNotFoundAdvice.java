package com.ykhamiakou.controller.advice;

import com.ykhamiakou.exception.PriceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Advice when price not found
 */
@ControllerAdvice
public class PriceNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(PriceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String categoryNotFoundHandler(PriceNotFoundException ex) {
        return ex.getMessage();
    }
}
