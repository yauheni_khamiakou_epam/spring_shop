package com.ykhamiakou.controller;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import com.ykhamiakou.exception.ProductNotFoundException;
import com.ykhamiakou.service.impl.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

/**
 * Product controller
 */
@RestController
public class ProductController {

    @Autowired
    private ProductServiceImpl productService;

    @GetMapping("/product")
    public Page<Product> all(Pageable pageable) {
        return this.productService.getAllProducts(pageable);
    }

    @GetMapping("/product/search/byId/{id}")
    public Product findById(@PathVariable Long id) throws ProductNotFoundException {
        return this.productService.findProductById(id).orElseThrow(()-> new ProductNotFoundException(id));
    }

    @GetMapping("/product/search/byName/{name}")
    public Product findByName(@PathVariable String name) throws ProductNotFoundException {
        return this.productService.findProductByName(name).orElseThrow(()-> new ProductNotFoundException(name));
    }

    @PostMapping("/product/search/byPrice")
    public Page<Product> findByPrice(@RequestBody Price price , Pageable pageable) {
        return this.productService.findProductByPrice(price, pageable);
    }

    @GetMapping("/product/search/byCategoryId/{category}")
    public Page<Product> findByCategoryId(@PathVariable Category category, Pageable pageable) {
        return this.productService.findProductByCategory(category, pageable);
    }

    @PostMapping("/product")
    public Product newProduct(@RequestBody Product product) {
        return this.productService.addProduct(product);
    }

    @DeleteMapping("/product/{id}")
    public void deleteProduct(@PathVariable Long id) {
        this.productService.deleteProductById(id);
    }

    @PutMapping("/product")
    public Product replaceProduct(@RequestBody Product product) {
        return this.productService.updateProduct(product);
    }
}
