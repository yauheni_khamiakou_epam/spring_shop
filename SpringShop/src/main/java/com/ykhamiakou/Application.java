package com.ykhamiakou;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.entity.Product;
import com.ykhamiakou.service.CategoryService;
import com.ykhamiakou.service.ProductService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.Map;

@SpringBootApplication
public class Application {

    /**
     * Main application method
     * @param args - args
     */
    public static void main(final String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    CommandLineRunner runner(final ProductService productService, final CategoryService categoryService) {
        return args -> {
            GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
            ctx.load("beanInitConfig.xml");
            ctx.refresh();
            Map<String, Category> categoryMap = ctx.getBeansOfType(Category.class);
            for(Map.Entry<String, Category> entry: categoryMap.entrySet()) {
                categoryService.addCategory(entry.getValue());
            }
            Map<String, Product> productMap = ctx.getBeansOfType(Product.class);
            for(Map.Entry<String, Product> entry: productMap.entrySet()) {
                productService.addProduct(entry.getValue());
            }
        };
    }

}