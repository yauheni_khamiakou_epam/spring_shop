package com.ykhamiakou.service;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Product service
 */
public interface ProductService {
    /**
     * Add new product
     * @param product - product to add
     * @return - added product
     */
    Product addProduct(final Product product);

    /**
     * Update some product
     * @param product - product to update
     * @return - updated product
     */
    Product updateProduct(final Product product);

    /**
     * Delete product
     * @param product - product to delete
     */
    void deleteProduct(final Product product);

    /**
     * Delete product by id
     * @param id - product id to delete
     */
    void deleteProductById(final Long id);

    /**
     * Get all products
     * @param pageable - pageable
     * @return - list with products
     */
    Page<Product> getAllProducts(final Pageable pageable);

    /**
     * Find product by it's name
     * @param name - product name
     * @return - products
     */
    Optional<Product> findProductByName(final String name);

    /**
     * Find products by category id
     * @param category - category
     * @param pageable - pageable
     * @return - products
     */
    Page<Product> findProductByCategory(final Category category, final Pageable pageable);

    /**
     * Find product by it's price
     * @param price - product price
     * @param pageable - pageable
     * @return - products
     */
    Page<Product> findProductByPrice(final Price price, final Pageable pageable);

    /**
     * Find product by it's id
     * @param id - product's id
     * @return - product
     */
    Optional<Product> findProductById(final Long id);
}
