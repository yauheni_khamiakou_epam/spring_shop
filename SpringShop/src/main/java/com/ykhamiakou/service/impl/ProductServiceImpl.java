package com.ykhamiakou.service.impl;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import com.ykhamiakou.repository.CategoryRepository;
import com.ykhamiakou.repository.PriceRepository;
import com.ykhamiakou.repository.ProductRepository;
import com.ykhamiakou.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Product service implementation
 */
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private PriceRepository priceRepository;

    @Override
    public Product addProduct(final Product product) {
        Optional<Product> executedProduct = this.productRepository.findByName(product.getName());
        if(executedProduct.isPresent()) {
            return executedProduct.get();
        }
        return this.productRepository.saveAndFlush(this.verityPrices(this.verifyCategories(product)));
    }

    /**
     * Verify product categories
     * @param product - product for verify
     * @return - verified product
     */
    private Product verifyCategories(final Product product) {
        Product verifiedProduct = product;
        Set<Category> categories = product.getCategories();
        if(!categories.isEmpty()) {
            Set<Category> verifiedCategories = new HashSet<>();
            for(Category category: categories) {
                Long id = category.getId();
                Category newOrEnteredCategory = category;
                if(id != null) {
                    Optional<Category> executedCategory = this.categoryRepository.findById(id);
                    if(executedCategory.isPresent()) {
                        newOrEnteredCategory = executedCategory.get();
                    }
                } else {
                    Optional<Category> executedCategory = this.categoryRepository.findByName(category.getName());
                    if(executedCategory.isPresent()) {
                        newOrEnteredCategory = executedCategory.get();
                    }
                }
                verifiedCategories.add(newOrEnteredCategory);
            }
            verifiedProduct.setCategories(verifiedCategories);
        }
        return verifiedProduct;
    }

    /**
     * Verify product prices
     * @param product - product for verify
     * @return - verified product
     */
    private Product verityPrices(final Product product) {
        Product verifiedProduct = product;
        Set<Price> prices = product.getPrices();
        if(!prices.isEmpty()) {
            Set<Price> verifiedPrices = new HashSet<>();
            for(Price price: prices) {
                Long id = price.getId();
                Price newOrEnteredPrice = price;
                if(id != null) {
                    Optional<Price> executedPrice = this.priceRepository.findById(id);
                    if(executedPrice.isPresent()) {
                        newOrEnteredPrice = executedPrice.get();
                    }
                }
                verifiedPrices.add(newOrEnteredPrice);
            }
            verifiedProduct.setPrices(verifiedPrices);
        }
        return verifiedProduct;
    }

    @Override
    public Product updateProduct(final Product product) {
        return this.productRepository.saveAndFlush(this.verityPrices(this.verifyCategories(product)));
    }

    @Override
    public void deleteProduct(final Product product) {
        this.productRepository.delete(product);
    }

    @Override
    public void deleteProductById(final Long id) {
        this.productRepository.deleteById(id);
    }

    @Override
    public Page<Product> getAllProducts(final Pageable pageable) {
        return this.productRepository.findAll(pageable);
    }

    @Override
    public Optional<Product> findProductByName(final String name) {
        return this.productRepository.findByName(name);
    }

    @Override
    public Page<Product> findProductByCategory(final Category category, final Pageable pageable) {
        return this.productRepository.findByCategories(category, pageable);
    }

    @Override
    public Page<Product> findProductByPrice(final Price price, final Pageable pageable) {
        return this.productRepository.findByPrices(price, pageable);
    }

    @Override
    public Optional<Product> findProductById(final Long id) {
        return this.productRepository.findById(id);
    }
}
