package com.ykhamiakou.service.impl;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.repository.CategoryRepository;
import com.ykhamiakou.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Category service implementation
 */
@Service
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category addCategory(final Category category) {
        Optional<Category> executedCategory = this.categoryRepository.findByName(category.getName());
        if(executedCategory.isPresent()) {
            return executedCategory.get();
        }
        Category superCategory = category.getSuperCategory();
        if(superCategory != null) {
            String superCategoryName = superCategory.getName();
            Optional<Category> executedSuperCategory = this.categoryRepository.findByName(superCategoryName);
            if(executedSuperCategory.isPresent()) {
                category.setSuperCategory(executedSuperCategory.get());
            }
        }
        return this.categoryRepository.saveAndFlush(category);
    }

    @Override
    public Category updateCategory(final Category category) {
        return this.categoryRepository.saveAndFlush(category);
    }

    @Override
    public void deleteCategory(final Category category) {
        this.categoryRepository.delete(category);
    }

    @Override
    public void deleteCategoryById(final Long id) {
        this.categoryRepository.deleteById(id);
    }

    @Override
    public Page<Category> getAllCategories(final Pageable pageable) {
        return this.categoryRepository.findAll(pageable);
    }

    @Override
    public Optional<Category> findCategoryById(final Long id) {
        return this.categoryRepository.findById(id);
    }

    @Override
    public Optional<Category> findCategoryByName(final String name) {
        return this.categoryRepository.findByName(name);
    }
}
