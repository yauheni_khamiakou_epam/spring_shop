package com.ykhamiakou.service.impl;

import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import com.ykhamiakou.repository.PriceRepository;
import com.ykhamiakou.service.PriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Optional;

/**
 * Price service implementation
 */
@Service
public class PriceServiceImpl implements PriceService {
    @Autowired
    private PriceRepository priceRepository;

    @Override
    public Price addPrice(final Price price) {
        return this.priceRepository.saveAndFlush(price);
    }

    @Override
    public Optional<Price> findPriceById(Long id) {
        return this.priceRepository.findById(id);
    }

    @Override
    public Price updatePrice(final Price price) {
        return this.priceRepository.saveAndFlush(price);
    }

    @Override
    public void deletePrice(final Price price) {
        this.priceRepository.delete(price);
    }

    @Override
    public void deletePriceById(final Long id) {
        this.priceRepository.deleteById(id);
    }

    @Override
    public Page<Price> getAllPrices(final Pageable pageable) {
        return this.priceRepository.findAll(pageable);
    }

    @Override
    public Page<Price> findPriceByCurrency(final Currency currency, final Pageable pageable) {
        return this.priceRepository.findByCurrency(currency, pageable);
    }

    @Override
    public Page<Price> findPriceByProducts(final Product product, final Pageable pageable) {
        return this.priceRepository.findByProducts(product, pageable);
    }

    @Override
    public Page<Price> findPriceByValueBetween(final BigDecimal value1, final BigDecimal value2, final Pageable pageable) {
        return this.priceRepository.findByValueBetween(value1, value2, pageable);
    }
}
