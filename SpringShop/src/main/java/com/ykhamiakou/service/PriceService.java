package com.ykhamiakou.service;

import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Optional;

/**
 * Price service
 */
public interface PriceService {
    /**
     * Add new price
     * @param price - price to add
     * @return - added price
     */
    Price addPrice(final Price price);

    /**
     * Update some price
     * @param price - price to update
     * @return - updated price
     */
    Price updatePrice(final Price price);

    /**
     * Delete price
     * @param price - price to delete
     */
    void deletePrice(final Price price);

    /**
     * Delete price by id
     * @param id - price id to delete
     */
    void deletePriceById(final Long id);

    /**
     * Get all prices
     * @param pageable - pageable
     * @return - list with prices
     */
    Page<Price> getAllPrices(final Pageable pageable);

    /**
     * Find prices by it's currency
     * @param currency - price currency
     * @param pageable - pageable
     * @return - prices
     */
    Page<Price> findPriceByCurrency(final Currency currency, final Pageable pageable);

    /**
     * Find prices by product
     * @param product - price product
     * @param pageable - pageable
     * @return - prices
     */
    Page<Price> findPriceByProducts(final Product product, final Pageable pageable);

    /**
     * Find prices by id
     * @param id - price id
     * @return - price
     */
    Optional<Price> findPriceById(final Long id);

    /**
     * Find prices by value range
     * @param value1 - value from
     * @param value2 - value to
     * @param pageable - pageable
     * @return - prices
     */
    Page<Price> findPriceByValueBetween(final BigDecimal value1, final BigDecimal value2, final Pageable pageable);
}
