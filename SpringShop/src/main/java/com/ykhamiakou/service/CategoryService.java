package com.ykhamiakou.service;

import com.ykhamiakou.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Category service
 */
public interface CategoryService {
    /**
     * Add new category
     * @param category - category to add
     * @return - added category
     */
    Category addCategory(final Category category);

    /**
     * Update some category
     * @param category - category to update
     * @return - updated category
     */
    Category updateCategory(final Category category);

    /**
     * Delete category
     * @param category - category to delete
     */
    void deleteCategory(final Category category);

    /**
     * Delete category by id
     * @param id - category id to delete
     */
    void deleteCategoryById(final Long id);

    /**
     * Get all categories
     * @param pageable - pageable
     * @return - list with categories
     */
    Page<Category> getAllCategories(final Pageable pageable);

    /**
     * Find category by it's id
     * @param id - category id
     * @return - category
     */
    Optional<Category> findCategoryById(final Long id);

    /**
     * Find category by it's name
     * @param name - category name
     * @return - category
     */
    Optional<Category> findCategoryByName(final String name);

}
