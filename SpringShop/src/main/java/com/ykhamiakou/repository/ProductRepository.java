package com.ykhamiakou.repository;

import com.ykhamiakou.entity.Category;
import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Product JPA Repository
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

    /**
     * Find product by it's name
     * @param name - product name
     * @return - product
     */
    Optional<Product> findByName(final String name);

    /**
     * Find product by category id
     * @param category - category
     * @param pageable - pageable
     * @return - product
     */
    Page<Product> findByCategories(final Category category, final  Pageable pageable);

    /**
     * Find product by it's price
     * @param prices - product price
     * @param pageable - pageable
     * @return - products
     */
    Page<Product> findByPrices(final Price prices, final Pageable pageable);
}
