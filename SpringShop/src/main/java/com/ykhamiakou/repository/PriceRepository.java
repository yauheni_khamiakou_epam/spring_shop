package com.ykhamiakou.repository;

import com.ykhamiakou.entity.Price;
import com.ykhamiakou.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.Currency;

/**
 * Price JPA Repository
 */
public interface PriceRepository extends JpaRepository<Price, Long> {
    /**
     * Find price by it's currency
     * @param currency - price currency
     * @param pageable - pageable
     * @return - product
     */
    Page<Price> findByCurrency(final Currency currency, final  Pageable pageable);

    /**
     * Find prices by product
     * @param products - price product
     * @param pageable - pageable
     * @return - prices
     */
    Page<Price> findByProducts(final Product products, final  Pageable pageable);

    /**
     * Find prices by value range
     * @param value1 - value from
     * @param value2 - value to
     * @param pageable - pageable
     * @return - prices
     */
    Page<Price> findByValueBetween(final BigDecimal value1, final  BigDecimal value2, final  Pageable pageable);

}
