package com.ykhamiakou.repository;

import com.ykhamiakou.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Category JPA Repository
 */
public interface CategoryRepository extends JpaRepository<Category, Long> {

    /**
     * Find category by it's name
     * @param name - category name
     * @return - category
     */
    Optional<Category> findByName(final String name);
}
