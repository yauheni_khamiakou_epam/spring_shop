package com.ykhamiakou.exception;

/**
 * Product searching exception
 */
public class ProductNotFoundException extends Exception {
    /**
     * Message constant
     */
    private static final String MESSAGE = "Product not found.";
    private static final String MESSAGE_NAME = "Product {} not found.";
    private static final String MESSAGE_ID = "Product by id {} not found.";

    /**
     * Constructor for message with product name
     * @param name - product id
     */
    public ProductNotFoundException(final String name) {
        super(String.format(ProductNotFoundException.MESSAGE_NAME, name));
            }

    /**
     * Constructor for message with product id
     * @param id - product id
     */
    public ProductNotFoundException(final Long id) {
        super(String.format(ProductNotFoundException.MESSAGE_ID, id));
    }

    public ProductNotFoundException() {
        super(ProductNotFoundException.MESSAGE);
    }
}
