package com.ykhamiakou.exception;

/**
 * Category searching exception
 */
public class CategoryNotFoundException extends Exception{
    /**
     * Message constant
     */
    private static final String MESSAGE = "Category not found.";
    private static final String MESSAGE_NAME = "Category {} not found.";
    private static final String MESSAGE_ID = "Category by id {} not found.";

    /**
     * Constructor for message with category name
     * @param name - category id
     */
    public CategoryNotFoundException(final String name) {
        super(String.format(CategoryNotFoundException.MESSAGE_NAME, name));
    }

    /**
     * Constructor for message with category id
     * @param id - category id
     */
    public CategoryNotFoundException(final Long id) {
        super(String.format(CategoryNotFoundException.MESSAGE_ID, id));
    }

    public CategoryNotFoundException() {
        super(CategoryNotFoundException.MESSAGE);
    }
}
