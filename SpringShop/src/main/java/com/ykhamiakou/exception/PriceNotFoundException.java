package com.ykhamiakou.exception;

/**
 * Price searching exception
 */
public class PriceNotFoundException extends Exception {
    /**
     * Message constant
     */
    private static final String MESSAGE = "Price not found.";

    public PriceNotFoundException() {
        super(PriceNotFoundException.MESSAGE);
    }
}
