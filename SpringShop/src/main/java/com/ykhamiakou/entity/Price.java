package com.ykhamiakou.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Shop price entity
 */
@Entity
@Table
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false)
    private BigDecimal value;
    @Column(nullable = false)
    private Currency currency;
    @ManyToMany(fetch = FetchType.LAZY,
                cascade = {CascadeType.ALL},
                mappedBy = "prices")
    @JsonIgnore
    private Set<Product> products = new HashSet<>();

    public Price() {
    }

    public Price(BigDecimal value, Currency currency) {
        this.value = value;
        this.currency = currency;
    }

    /**
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return value
     */
    public BigDecimal getValue() {
        return this.value;
    }

    /**
     * @param value to set
     */
    public void setValue(final BigDecimal value) {
        this.value = value;
    }

    /**
     * @return currency
     */
    public Currency getCurrency() {
        return this.currency;
    }

    /**
     * @param currency to set
     */
    public void setCurrency(final Currency currency) {
        this.currency = currency;
    }

    /**
     * @return products
     */
    public Set<Product> getProducts() {
        return this.products;
    }

    /**
     * @param products to set
     */
    public void setProducts(final Set<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return Objects.equals(id, price.id) &&
                Objects.equals(value, price.value) &&
                Objects.equals(currency, price.currency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, value, currency);
    }
}
