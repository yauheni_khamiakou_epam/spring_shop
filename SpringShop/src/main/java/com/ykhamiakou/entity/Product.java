package com.ykhamiakou.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Shop product entity
 */
@Entity
@Table
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false,
            unique = true)
    private String name;
    @ManyToMany(fetch = FetchType.EAGER,
                cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinTable(name = "products_categories",
                joinColumns = {@JoinColumn(name = "product_id", referencedColumnName = "id")},
                inverseJoinColumns = {@JoinColumn(name = "category_id", referencedColumnName = "id")})
    private Set<Category> categories = new HashSet<>();
    @ManyToMany(fetch = FetchType.EAGER,
                cascade = {CascadeType.ALL})
    @JoinTable(name = "products_prices",
            joinColumns = {@JoinColumn(name = "product_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "price_id", referencedColumnName = "id")})
    private Set<Price> prices = new HashSet<>();

    public Product() {
    }

    public Product(String name) {
        this.name = name;
    }

    /**
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return categories
     */
    public Set<Category> getCategories() {
        return this.categories;
    }

    /**
     * @param categories to set
     */
    public void setCategories(final Set<Category> categories) {
        this.categories = categories;
    }

    /**
     * @return prices
     */
    public Set<Price> getPrices() {
        return this.prices;
    }

    /**
     * @param prices to set
     */
    public void setPrices(final Set<Price> prices) {
        this.prices = prices;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id) &&
                Objects.equals(name, product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
