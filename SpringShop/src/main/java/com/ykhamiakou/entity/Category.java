package com.ykhamiakou.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Shop product category entity
 */
@Entity
@Table
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(nullable = false,
            unique = true)
    private String name;
    @ManyToOne(fetch = FetchType.EAGER,
               cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    private Category superCategory;
    @OneToMany(fetch = FetchType.LAZY,
               mappedBy = "superCategory")
    @JsonIgnore
    private Set<Category> subCategories;
    @ManyToMany(fetch = FetchType.LAZY,
                mappedBy = "categories")
    @JsonIgnore
    private Set<Product> products = new HashSet<>();

    public Category() {
    }

    public Category(String name, Category superCategory) {
        this.name = name;
        this.superCategory = superCategory;
    }

    /**
     * @return id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @param name to set
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return superCategory
     */
    public Category getSuperCategory() {
        return this.superCategory;
    }

    /**
     * @param superCategory to set
     */
    public void setSuperCategory(final Category superCategory) {
        this.superCategory = superCategory;
    }

    /**
     * @return subCategories
     */
    public Set<Category> getSubCategories() {
        return this.subCategories;
    }

    /**
     * @param subCategories to set
     */
    public void setSubCategories(final Set<Category> subCategories) {
        this.subCategories = subCategories;
    }

    /**
     * @return products
     */
    public Set<Product> getProducts() {
        return this.products;
    }

    /**
     * @param products to set
     */
    public void setProducts(final Set<Product> products) {
        this.products = products;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(id, category.id) &&
                Objects.equals(name, category.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

}
